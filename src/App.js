import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Bar, Doughnut, Line} from 'react-chartjs-2';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      lineChartData: {
          labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL"],
          datasets: [{
            label: 'Revenue Protected',
            fill: false,
            lineTension: 0.4,
            backgroundColor: '#E64688',
            borderColor: '#E64688',
            borderCapStyle: 'butt',
            borderDashOffset: 0.0,
            borderKoinStyle: 'miter',
            pointBorderColor: '#E64688',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHovorBackgroundColor: '#E64688',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            pointStyle: 'star',
            data: [1, 2, 1.5, 2.8, 2.5, 3.3, 3.5],
          }]
      }
    }
  }



  render() {
      const data = (canvas) => {
        const ctx = canvas.getContext('2d');
        const gradient = ctx.createLinearGradient(0,0,420,0);
        gradient.addColorStop(0, '#45DCE5');
        gradient.addColorStop(1, '#8287ED');

        return {
          labels: ["Transactions Protected", "Not Protected"],
          datasets: [{
            label: "An Awesome Doughnut Chart",
            backgroundColor: [
              gradient,
            ],
            borderWidth: 0,
            data: [80, 20],
          }],
        }
      }
    return (
      <div className="App">
        <div className="chart-header">
          <h1>Practicing Chart.js</h1>
        </div>
        <div className="charts">
          <div className="lineGraph">
              <Line
                  data={this.state.lineChartData}
                  options={{
                    maintainAspectRatio: false,
                    legend: {
                      display: false
                    },
                    scales: {
                      xAxes: [{
                        gridLines: {
                          display: false
                        },
                        ticks: {
                          fontColor:'#9799A8'
                        },
                      }],
                      yAxes: [{
                        gridLines: {
                          color: '#9799A8',
                          drawBorder: false,
                        },
                        ticks: {
                          fontColor:'#9799A8',
                        },
                        scaleLabel: {
                          display: true,
                          fontColor: '#9799A8',
                          labelString: 'Percent %'
                      }
                      }],
                    }

                  }}
              />
          </div>
          <div className="doughnut">
              <Doughnut
                data={data}
                width={50}
                height={25}
                options={{
                  maintainAspectRatio: false,
                  cutoutPercentage: 90
                }}
              />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
